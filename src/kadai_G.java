import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class kadai_G {
    private JTextArea textArea1_item;
    private JButton buttonCheckOut;
    private JPanel root;
    private JButton Whiskey;
    private JButton Brandy;
    private JButton Cocktail;
    private JButton DomPerignon;
    private JButton ChampagneTower;
    private JButton DivaVodka;
    private JLabel total;

    public static void main(String[] args) {
        JFrame frame = new JFrame("kadai_G");
        frame.setContentPane(new kadai_G().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String name, int cost) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + name + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + name + "!.It will be served as soon as possible.");

            int a = calculation(cost);
            String currentText = textArea1_item.getText();
            textArea1_item.setText(currentText + name + cost + "yen\n");
            total.setText(String.valueOf(a));
        }
    }


    public int calculation(int cost) {
        int goukei = Integer.parseInt(total.getText());
        return goukei + cost;
    }

    public void checkout() {

        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to checkout?  ",
                "Order Comfirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (confirmation == 0) {
            int Checkout = JOptionPane.showConfirmDialog(
                    null,
                    "Thank you. The root price is " + calculation(0) + " yen.",
                    "Message",
                    JOptionPane.OK_CANCEL_OPTION
            );

            if (Checkout == 0) {

                textArea1_item.setText(null);
                total.setText("0");

            }
        }
    }

    public kadai_G() {
        Whiskey.setIcon(new javax.swing.ImageIcon(".\\\\src\\\\whisky.jpg"));
        Whiskey.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Whiskey", 2000);

            }
        });
        Brandy.setIcon(new javax.swing.ImageIcon(".\\\\src\\\\Brandy.jpg"));
        Brandy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Brandy", 3000);
            }
        });
        Cocktail.setIcon(new javax.swing.ImageIcon(".\\\\src\\\\Cocktail.jpg"));
        Cocktail.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cocktail", 4000);
            }
        });
        DomPerignon.setIcon(new ImageIcon(".\\\\src\\\\domperi.jpg"));
        DomPerignon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("DomPerignon", 400000);

            }
        });

        ChampagneTower.setIcon(new ImageIcon(".\\\\src\\\\shanpan.jpg"));
        ChampagneTower.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ChampagneTower", 5000000);

            }
        });
        DivaVodka.setIcon(new ImageIcon(".\\\\src\\\\DivaVodka3.jpg"));
        DivaVodka.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("DivaVodka", 100000000);



            }
        });
        buttonCheckOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkout();

            }
        });
    }


}

